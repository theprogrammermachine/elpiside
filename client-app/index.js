
import React from "react";
import { render } from "react-dom";
import MonacoEditor, { MonacoDiffEditor } from "react-monaco-editor";
import {handleCurrentFilePathReset, handleFileOpen, FileView} from './FileView';
import ClassView from './ClassView';
import MainAppBar from './MainAppBar';
import Websocket from 'react-websocket';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PropTypes, { bool } from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { css } from 'glamor';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import yellow from '@material-ui/core/colors/yellow';
import Paper from '@material-ui/core/Paper';
import DevicesIcon from '@material-ui/icons/Devices';
import * as monaco from 'monaco-editor/esm/vs/editor/editor.main';
import { Button } from "@material-ui/core";
import { blue } from "@material-ui/core/colors";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

let monarchTokenProvider;
let keywords = [
  'use', 'command', 'times', 'instance', 'of', 'define', 'return', 'prop', "on", "created", 'class', 'function', 'with', 'params', 'for', 'until', 'if', 'else', 'then', 'do', 'true', 'false', 'not'
];

// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

class CodeEditor extends React.Component {

  terminalLines = [];
  currentFilePath = '';

  setCurrentFilePath(cfp) {
    this.currentFilePath = cfp;
  }

  updateDimensions() {
    var w = window,
      d = document,
      documentElement = d.documentElement,
      body = d.getElementsByTagName('body')[0],
      width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
      height = w.innerHeight|| documentElement.clientHeight|| body.clientHeight;
      this.setState({width: width, height: height});
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
    let that = this;
    window.addEventListener("keydown", function(e) {
      if ((window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)  && e.keyCode == 83) {
        e.preventDefault();
        if (that.currentFilePath.length > 0) {
          delete that.unsavedTabs[that.currentFilePath]
          if (!toast.isActive(that.toastId))
            that.toastId = toast("Saving...");
          const options = {
            method: 'post',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "path": that.currentFilePath,
              "content": that.editor.getValue()
            })
          };
          fetch("../update-code", options);
        }
      }
      else if ((window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)  && e.keyCode == 67) {
        if (that.state.terminalInputFocused) {
          e.preventDefault();
          const options = {
            method: 'post',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            }
          };
          fetch("../kill-current", options);
        }
      }
      else if (e.keyCode == 13) {
        if (!that.state.terminalIsBusy && that.state.terminalInputFocused) {
        that.terminalLines.push({type: 'me', content: that.state.terminalPrompt + '$ ' + 
                                that.terminalInput.value});
        if (that.terminalInput.value.startsWith('cd ')) {
          const options = { 
            method: 'post',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "path": that.terminalInput.value.substring(3)
            })
          };
          fetch("../cd", options)
           .then(res => {});
        }
        else {
          const options = { 
            method: 'post',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "command": that.terminalInput.value
            })
          };
          fetch("../run-command", options)
            .then(res => {});
        }
        that.terminalBottom.scrollIntoView(false);
        that.terminalInput.value = '';
      }
    }
    }, false);
  }
  
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  setSideView(sv) {
    this.setState({sideView: sv});
  }

  changingValue = false;

  setCode(code) {
    this.changingValue = true;
    this.setState({code: code}, () => {
      if (this.currentFilePath in this.posesDict) {
        let currentPos = this.posesDict[this.currentFilePath];
        this.editor.setPosition({column: currentPos.column, lineNumber: currentPos.lineNumber});
        this.editor.revealLineInCenter(currentPos.lineNumber);
      }
      else {
        this.editor.setPosition({column: 0, lineNumber: 1});
        this.editor.revealLineInCenter(1);
      }
      this.changingValue = false;
      this.buildClasses();
      this.findKeywords();
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      code: "// type your code... \n",
      theme: "vs-dark",
      width: 0,
      height: 0,
      zIndexTarget: '',
      sideView: 1,
      terminalContent: [],
      terminalPrompt: '',
      fileTree: {
        name: 'root',
        children: []
      },
      componentsTree: [],
      openTabs: [],
      terminalInputFocused: false,
      terminalIsBusy: false,
      tabValue: 0,
      editorBc: '#022c43',
      minimapBc: '#115173',
      appbarBc: '#115173',
      terminalBc: '#053f5e',
      fileViewBc: '#053f5e',
      classViewBc: '#022c43'
    };
    this.updateDimensions = this.updateDimensions.bind(this);
    this.setSideView = this.setSideView.bind(this);
    this.setCode = this.setCode.bind(this);
    this.setCurrentFilePath = this.setCurrentFilePath.bind(this);
    this.buildClasses = this.buildClasses.bind(this);
    this.onTerminalInputFocus = this.onTerminalInputFocus.bind(this);
    this.onTerminalInputBlur = this.onTerminalInputBlur.bind(this);
    this.addTab = this.addTab.bind(this);
    this.clearTabs = this.clearTabs.bind(this);
    monaco.languages.register({ id: 'Elpis' });

monarchTokenProvider = {
	
  keywords: keywords,

  typeKeywords: [
    
  ],

  operators: [
    '=', '>', '<', '!', '~', '?', ':', '==', '<=', '>=', '!=',
    '++', '--', '+', '-', '*', '/', '^',
    '<<', '>>', '>>>', '+=', '-=', '*=', '/=', '&=', '|=', '^=',
    'mod=', '<<=', '>>=', '>>>='
  ],

  // we include these common regular expressions
  symbols:  /[=><!~?:&|+\-*\/\^%]+/,

  // C# style strings
  escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

  // The main tokenizer for our languages
  tokenizer: {
    root: [

      // strings
      [/"/,  { token: 'string.quote', bracket: '@open', next: '@string' } ],

      [/define[\s]+command[\s]+{.*/, { token: "command"}],

      // identifiers and keywords
      [/[a-z_$][\w$]*/, { cases: { '@typeKeywords': 'keyword',
                                   '@keywords': 'keyword',
                                   '@default': 'identifier' } }],
      [/[A-Z][\w\$]*/, 'type.identifier' ],  // to show class names nicely

      // whitespace
      { include: '@whitespace' },

      // delimiters and operators
      [/[{}()\[\]]/, 'brackets'],
      [/[<>](?!@symbols)/, 'brackets'],
      [/@symbols/, { cases: { '@operators': 'operator',
                              '@default'  : '' } } ],

      // @ annotations.
      // As an example, we emit a debugging log message on these tokens.
      // Note: message are supressed during the first load -- change some lines to see them.
      [/@\s*[a-zA-Z_\$][\w\$]*/, { token: 'annotation' }],

      // numbers
      [/\d*\.\d+([eE][\-+]?\d+)?/, 'number.float'],
      [/0[xX][0-9a-fA-F]+/, 'number.hex'],
      [/\d+/, 'number'],

      // delimiter: after number because of .\d floats
      [/[;,.]/, 'delimiter'],
    ],

    comment2: [
      [/[^\/*]+/, 'comment2' ],
      [/\/\*/,    'comment2', '@push' ],   
      ["\\*/",    'comment2', '@pop'  ],
      [/[\/*]/,   'comment2' ]
    ],

    string: [
      [/[^\\"]+/,  'string'],
      [/@escapes/, 'string.escape'],
      [/\\./,      'string.escape.invalid'],
      [/"/,        { token: 'string.quote', bracket: '@close', next: '@pop' } ]
    ],

    whitespace: [
      [/[ \t\r\n]+/, 'white'],
      [/\/\*/,       'comment2', '@comment2' ],
      [/\/\/.*$/,    'comment2']
    ],
  },
}

monaco.languages.setMonarchTokensProvider('Elpis', monarchTokenProvider);

monaco.editor.defineTheme('ElpisTheme', {
	base: 'vs-dark',
	inherit: true,
	rules: [
    { token: 'keyword', foreground: 'FFA500', fontStyle: 'bold' },
    { token: 'operator', foreground: 'FFA500', fontStyle: 'bold' },
    { token: 'brackets', foreground: '00FFFF', fontStyle: 'bold' },
    { token: 'command', foreground: '0099FF', fontStyle: 'bold' },
    { token: 'comment2', foreground: '00FF99', fontStyle: 'bold' },
    { background: this.state.minimapBc}
	],
  colors: {
    'editor.background': this.state.editorBc,
    'editor.foreground': '#d9d7ce',
    'editorIndentGuide.background': '#393b41',
    'editorIndentGuide.activeBackground': '#494b51',
  },
});
monaco.languages.registerCompletionItemProvider('Elpis', {
	provideCompletionItems: () => {
		var suggestions = [{
			label: 'ifelse',
			kind: monaco.languages.CompletionItemKind.Snippet,
			insertText: [
				'if ${1:condition} then do',
				'\t$0',
				'else do',
				'\t',
				''
			].join('\n'),
			insertTextRules: monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
			documentation: 'If-Else Statement'
		}, {
			label: 'ifelseif',
			kind: monaco.languages.CompletionItemKind.Snippet,
			insertText: [
				'if ${1:condition} then do',
				'\t$0',
				'else if ${2:condition} then do',
				'\t',
				''
			].join('\n'),
			insertTextRules: monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
			documentation: 'If-ElseIf Statement'
		}, {
			label: 'ifelseIfelse',
			kind: monaco.languages.CompletionItemKind.Snippet,
			insertText: [
				'if ${1:condition} then do',
        '\t$0',
        'else if ${2:condition} then do',
				'\t',
				'else do',
				'\t',
				''
			].join('\n'),
			insertTextRules: monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
			documentation: 'If-Else Statement'
		}];
		return { suggestions: suggestions };
	}
});
  }

  posesDict = {};

  editorDidMount = editor => {
    this.editor = editor;
    this.editor.getModel().updateOptions({ tabSize: 2 });
    this.editor.onDidType(text => {
      this.unsavedTabs[this.currentFilePath] = true;
      if (text === '\n') {
        var line = editor.getPosition();
        var text = editor.getValue(line);
        var splitedText=text.split("\n");
        var l = splitedText[line.lineNumber-2];
        if (l.trimRight().endsWith(' do') || l.trimRight().endsWith(' containing') ||
            l.trim().startsWith('define class') || l.trim().startsWith('define function')) {
          var range = new monaco.Range(line.lineNumber, 1, line.lineNumber, 1);
          var id = { major: 1, minor: 1 };
          var text = "  ";
          var op = {identifier: id, range: range, text: text, forceMoveMarkers: true};
          editor.executeEdits("my-source", [op]);
        }
      }
    });
    this.findKeywords();
    this.editor.onDidChangeCursorPosition((e) => {
      if (!this.changingValue)
        this.posesDict[this.currentFilePath] = this.editor.getPosition();
    });
  };

  setDarkTheme = () => {
    this.setState({ theme: "vs-dark" });
  };

  setLightTheme = () => {
    this.setState({ theme: "vs-light" });
  };

  onTerminalInputFocus() {
    this.setState({terminalInputFocused: true});
  }

  onTerminalInputBlur() {
    this.setState({terminalInputFocused: false});
  }

  savedEditorFirstState = false;

  buildClasses() {
    let string = false;
                  let listOfClasses = [];
                  for (let counter = 0; counter < this.state.code.length; counter++) {
                    if (this.state.code.charAt(counter) === '\\"') {
                      continue;
                    }
                    else if (this.state.code.charAt(counter) === '"') {
                      string = !string;
                      continue;
                    }
                    if (!string && this.state.code.substring(counter).startsWith('define')) {
                      let lineStart = counter;
                      let spaceCount = 0;
                      while (lineStart > 0 && this.state.code.charAt(lineStart - 1) === ' ') {
                        spaceCount++;
                        lineStart--;
                      }
                      counter += 'define '.length;
                      let partly = this.state.code.substring(counter);
                      let type = '';
                      if (partly.startsWith('class')) {
                        type = 'class';
                        counter += 'class '.length;
                        partly = this.state.code.substring(counter);
                      }
                      else if (partly.startsWith('function')) {
                        type = 'function';
                        counter += 'function '.length;
                        partly = this.state.code.substring(counter);
                      }
                      else {
                        continue;
                      }
                      let name = '';
                      try {
                        name = partly.match(/^[\w\d]+/gs)[0];
                      } catch(ex) {}
                      listOfClasses.push({spaceCount: spaceCount, name: name, type: type});
                    }
                  }
                  let components = [];
                  let prevItem = null;
                  let pointingItem = null;
                  listOfClasses.forEach((item) => {
                    item.children = [];
                    item.parent = null;
                    if (prevItem === null) {
                      pointingItem = item;
                      components.push(item);
                    }
                    else {
                      if (item.spaceCount > pointingItem.spaceCount) {
                        pointingItem.children.push(item);
                        item.parent = pointingItem;
                      }
                      else if (item.spaceCount === pointingItem.spaceCount) {
                        pointingItem = pointingItem.parent;
                        if (pointingItem === null) {
                          pointingItem = item;
                          components.push(item);
                        }
                        else {
                          pointingItem.children.push(item);
                          item.parent = pointingItem;
                        }
                      }
                      else if (item.spaceCount < pointingItem.spaceCount) {
                        while (pointingItem.spaceCount >= item.spaceCount) {
                          pointingItem = pointingItem.parent;
                        }
                        if (pointingItem === null) {
                          pointingItem = item;
                          components.push(item);
                        }
                        else {
                          pointingItem.children.push(item);
                          item.parent = pointingItem;
                        }
                      }
                    }
                    prevItem = item;
                  });
                  let rootObj = {name: 'components', children: components}
                  this.setState({componentsTree: rootObj});
  };

  addTab(name, path) {
    let openTabs = this.state.openTabs;
    let tabIsOpen = false;
    openTabs.forEach((tab) => {
      if (tabIsOpen) return;
      if (tab.path === path) {
        tabIsOpen = true;
        return;
      }
    });
    if (!tabIsOpen) {
      openTabs.push({path: path, name: name});
      this.setState({openTabs: openTabs, tabValue: openTabs.length - 1});
    }
  }

  clearTabs() {
    this.setState({openTabs: [], tabValue: 0});
  }

  findKeywords() {
      let occ = this.editor.getValue().match(/(\bdefine[\s]+command[\s]+{[\s]+[A-Za-z0-9 ]+[\s]+})/g);
      if (occ !== null) {
        let filtered = [];
        occ.forEach(item => {
          item = item.trim().substring('define'.length);
          item = item.trim().substring('command'.length);
          item = item.trim().substring(1, item.length - 1);
          let items = item.trim().split(' ');
          items.forEach(miniItem => {
            if (miniItem.trim().length > 0)
              filtered.push(miniItem.trim());
          });
        });
        if (filtered.length > 0) {
          let newKeywords = [];
          keywords.forEach(keyword => {
            newKeywords.push(keyword);
          });
          let newFound = false;
          filtered.forEach(item => {
            if (!newKeywords.includes(item)) {
              newFound = true;
              newKeywords.push(item);
            }
          });
          if (!newKeywords.equals(monarchTokenProvider.keywords)) {
            monarchTokenProvider.keywords = newKeywords;
            monaco.languages.setMonarchTokensProvider('Elpis', monarchTokenProvider);
          }
        }
      }
  }

  unsavedTabs = {};

  render() {
    const { code, theme } = this.state;
    const options = {
      selectOnLineNumbers: true,
      roundedSelection: true,
      readOnly: false,
      cursorStyle: "line",
      automaticLayout: true
    };
    function a11yProps(index) {
      return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
      };
    }

    const appTheme = createMuiTheme({
      palette: {
        secondary: yellow,
      },
    });

    let tabCounter = 0;

    const closeTab = () => {
      delete this.unsavedTabs[this.currentFilePath];
      let openTabs = this.state.openTabs;
            let index = this.closingTabIndex;

            let openTabClosing = false;
            if (this.state.tabValue === index) {
              openTabClosing = true;
              this.setCurrentFilePath('');
              handleCurrentFilePathReset();
              this.setCode('');
            }
            openTabs.splice(index, 1);
            let finalIndex = openTabClosing ? 
                openTabs.length - 1 : 
                this.state.tabValue > openTabs.length - 1 ?
                    openTabs.length - 1 :
                    this.closingTabIndex > this.state.tabValue ?
                        this.state.tabValue :
                        this.state.tabValue - 1;
            this.setState({
              openTabs: openTabs,
              tabValue: finalIndex
            });
            let t = this.state.openTabs[finalIndex];
            if (t !== undefined) {
              handleFileOpen(t, this.setCurrentFilePath, this.setCode);
            }
    }

    return (
      <div>
        <Websocket url='ws://localhost:3001/'
              onMessage={(data) => {
                let packet = JSON.parse(data);
                if (packet.type === 'terminal') {
                  this.terminalLines.push({type: 'system', content: packet.content});
                  this.terminalBottom.scrollIntoView(false);
                  this.setState({terminalPrompt: packet.path});
                  if (!this.state.terminalIsBusy) {
                    this.setState({terminalIsBusy: true});
                  }
                }
                else if (packet.type === 'terminal-close') {
                  this.setState({terminalIsBusy: false});
                }
                else if (packet.type === 'files') {
                  this.setState({fileTree: packet.tree});
                }
              }}/>
          <div>
            <ThemeProvider theme={appTheme}>
            <Paper elevation={12} style={{
              position: 'absolute',
              left: 0,
              top: 0,
              backgroundColor: this.state.minimapBc,
              width: 64,
              height: this.state.height + 16 + 'px'
            }}>
              <div
                style={{
                  width: '100%',
                  height: 24
                }}/>
              <IconButton 
                onClick={() => {
                  if (this.state.sideView === 1) {
                    this.setSideView(0);  
                    return;
                  }
                  this.setSideView(1)
                }}
                style={{
                  marginLeft: 8
                }}>
                  <DevicesIcon 
                    style={{
                      fill: this.state.sideView === 1 ? 'yellow' : 'white'
                    }}/>
              </IconButton>
              <IconButton
                onClick={() => {
                  if (this.state.sideView === 2) {
                    this.setSideView(0);  
                    return;
                  }
                  this.setSideView(2)
                }}
                style={{
                  marginLeft: 8
                }}>
                  <DevicesIcon style={{
                    fill: this.state.sideView === 2 ? 'yellow' : 'white'
                  }}/>
              </IconButton>
            </Paper>
            </ThemeProvider>
            <div style={{
              position: 'absolute',
              left: 56,
              top: 0,
              width: this.state.width ? 'calc(100% - 56px)' : 'calc(100% - 56px)'
            }}>
              <MainAppBar parent={this}/>
              <div
                style={{
                  width: '100%',
                  height: this.state.height ? '100%' : '100%',
                  display: 'flex',
                  overflow: 'hidden'
                }}>
                <div
            style={{
              width: this.state.sideView === 1 ? 300 : 0,
              height: this.state.height + 'px',
              backgroundColor: this.state.fileViewBc,
            }}>
            <div
              style={{
                width: this.state.sideView > 0 ? '100%' : 0,
                height: this.state.height ? 'calc(100% - 380px)' : 'calc(100% - 380px)'
              }}>
                <FileView addTab={this.addTab} fileSetter={this.setCurrentFilePath} codeSetter={this.setCode} treeDisplay={this.state.sideView === 1} 
                  obj={this.state.fileTree}/>
            </div>
            <div
              style={{
                width: this.state.sideView > 0 ? '100%' : 0,
                height: 400,
                paddingTop: 32,
                backgroundColor: this.state.classViewBc
              }}>
                <ClassView treeDisplay={this.state.sideView === 1} obj={this.state.componentsTree}/>
            </div>
            </div>
                <div
              style={{
                width: this.state.sideView === 2 ? 450 : 0,
                height: this.state.height + 'px',
                backgroundColor: this.state.fileViewBc,
              }}>
                <div style={{
                  width: 'calc(100% - 32px)',
                  height: 'calc(100% - 56px - 32px)',
                  backgroundColor: 'black',
                  margin: 16
                }}>
                  <div style={{width: '100%', height: 'calc(100% - 40px - 16px - 16px)', backgroundColor: 'white'}} id={'emulator'}></div>
                  <ThemeProvider theme={appTheme}>
                    <Button variant="contained" color="secondary" style={{display: this.state.sideView === 2 ? 'block' : 'none',
                     marginLeft: 16, marginTop: 16, height: 40}}
                          onClick={() => {
                            document.getElementById("emulator").innerHTML=
                            '<object width="100%" height="100%" type="text/html" data="http://localhost:8080/webapp/index.html" ></object>';
                          }}>Reload</Button>
                  </ThemeProvider>
                </div>
              </div>
        <div style={{
          width: this.state.width ?
                 this.state.sideView === 1 ? 'calc(100% - 300px)' : this.state.sideView === 2 ? 'calc(100% - 450px)' : '100%'  :
                 this.state.sideView === 1 ? 'calc(100% - 300px)' : this.state.sideView === 2 ? 'calc(100% - 450px)' : '100%'
        }}>
          <AppBar position="static" style={{height: 56, backgroundColor: this.state.terminalBc}}>
            <ThemeProvider theme={appTheme}>
            <Tabs value={this.state.tabValue} onChange={(e, newValue) => {
              if (e.target.tagName !== 'DIV') {
                e.preventDefault();
                return;
              }
              let tab = this.state.openTabs[newValue];
              if (tab === undefined) {
                if (this.state.openTabs.length > 0) {
                  newValue = this.state.openTabs.length - 1;
                  tab = this.state.openTabs[newValue];
                }
              }
              if (tab !== undefined) {
                this.setState({tabValue: newValue});
                handleFileOpen(tab, this.setCurrentFilePath, this.setCode);
              }
            }} aria-label="tabs">
              {
                this.state.openTabs.map((tab) => {
                  const tabIndex = tabCounter;
                  tabCounter++;
                  return (<Tab label={
                  <div>
                    {tab.name}
                    <IconButton
                        onClick={(e) => {
                          e.preventDefault();
                          this.closingTabIndex = tabIndex;
                          if (this.currentFilePath.length > 0) {
                            if (this.unsavedTabs[this.currentFilePath] === true) {
                              this.savingFile = this.currentFilePath;
                              this.setState({saveOpen: true});
                            }
                            else {
                              closeTab();
                            }
                          }
                        }}
                        color="primary" aria-label="close" component="span">
                        <CloseIcon color='secondary'/>
                    </IconButton>
                  </div>
               } {...a11yProps(tabIndex)}/>);
                })
              }
            </Tabs>
            </ThemeProvider>
          </AppBar>
          <MonacoEditor
            width={'100%'}
            height={this.state.height ? 'calc(100% - 364px - 56px)' : 'calc(100% - 364px - 56px)'}
            language={'Elpis'}
            style={{
              overflow: 'hidden'
            }}
            value={code}
            options={options}
            onChange={() => {
              if (this.editor !== null)
                this.setState({code: this.editor.getValue()}, () => {
                  this.buildClasses();
                  this.findKeywords();
                });
            }}
            editorDidMount={this.editorDidMount}
            theme={'ElpisTheme'}
          />
          <div 
            onClick={() => this.terminalInput.focus()}
            style={{
              width: this.state.width ? '100%' : '100%',
              display: 'flex',
              backgroundColor: this.state.terminalBc,
              height: 308,
              overflowY: 'auto',
              cursor: 'text'
            }}>
            <ul
              id={'terminal'}
              ref={(el) => { this.terminal = el; }} 
              style={{
                listStyleType: 'none',
                color: 'white'
              }}>
              {
                this.terminalLines.map((line) => {
                  if (line.type === 'system') {
                    return (<li style={{ color: 'white' }}>
                              <pre>
                                {line.content}
                              </pre>
                            </li>);
                  }
                  else {
                    return (<li style={{ color: '#ffd700' }}>
                            <pre>
                                {line.content}
                            </pre>
                          </li>);
                  }
                })
              }
              <div style={{
                display: 'flex',
                color: 'white',
              }}>
              {this.state.terminalIsBusy ? null :
                <div style={{color: '#ffd700'}}
                  ref={(el) => {this.terminalPrompt = el;}}>
                    {this.state.terminalPrompt}
                </div>
              }
              <input type="text"
                  style={{
                    marginBottom: 32,
                    background: 'transparent',
                    border: 'none',
                    color: 'white',
                    outline: 'none',
                    marginLeft: 16
                  }}
                  onFocus={this.onTerminalInputFocus}
                  onBlur={this.onTerminalInputBlur}
                  ref={(el) => {this.terminalInput = el;}}
              />
              </div>
              <div
                ref={(el) => {this.terminalBottom = el;}}
                style={{
                  width: '100%',
                  height: 16
                }}/>
            </ul>
          </div>
        </div>
        </div>
              <ToastContainer
                toastClassName={css({
            background: '#003a45'
          })}
          closeButton={false}
                autoClose={2000}
              />
            </div>
          </div>
      <Dialog
        open={this.state.saveOpen}
        onClose={() => this.setState({saveOpen: false})}
        aria-labelledby="responsive-dialog-title">
        <DialogTitle id="responsive-dialog-title">{"Save file"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you want to save {this.savingFile} ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => {
            
            closeTab();

            this.savingFile = null;
            this.setState({saveOpen: false});
          }} color="primary">
            Don't save
          </Button>
          <Button onClick={() => {
            if (this.currentFilePath.length > 0) {
              const options = {
                method: 'post',
                headers: {
                  'Accept': 'application/json, text/plain, */*',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "path": this.currentFilePath,
                  "content": this.editor.getValue()
                })
              };
              fetch("../update-code", options);
            }

            closeTab();

            this.savingFile = null;
            this.setState({saveOpen: false});
          }} color="primary" autoFocus>
            Save it
          </Button>
        </DialogActions>
      </Dialog>
      </div>
    );
  }
}

const App = () => <CodeEditor />;

render(<App />, document.getElementById("root"));
