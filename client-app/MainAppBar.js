import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import {openCreateFile} from './FileView';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {handleSwitchProject} from './FileView';

import RunIcon from './images/run.png';
import PauseIcon from './images/pause.png';
import StopIcon from './images/stop.png';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        color: 'white',
        marginRight: theme.spacing(2),
    },
    title: {
        color: 'white',
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    leftMenu: {
        marginLeft: -16,
        [theme.breakpoints.up('sm')]: {
            marginLeft: 32,
        },
    },
    search: {
        color: 'white',
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: 178,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 120,
            '&:focus': {
                width: 200,
            },
        },
    },
}));

let createOpen = null;
let setCreateOpen = null;
let projectsOpen = null;
let setProjectsOpen = null;
let createFileNameValue = '';
let projectTypeValue = null;
let setProjectTypeValue = null;

export default function MainAppBar(props) {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);
    [createOpen, setCreateOpen] = React.useState(false);
    [projectsOpen, setProjectsOpen] = React.useState(false);
    const [projectsList, setProjectsList] = React.useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const onOptionMenuItemSelected = (index) => {
        if (index === 0) {
            handleClose();
            setCreateOpen(true);
        }
        else if (index === 1) {
            handleClose();
            setProjectsOpen(true);
            const options = { 
                method: 'post',
                headers: {
                  'Accept': 'application/json, text/plain, */*',
                  'Content-Type': 'application/json'
                }
              };
              fetch("../get-projects", options)
                .then(rawRes => rawRes.json())
                .then(res => {
                   setProjectsList(res);
                });
        }
        else if (index === 2) {
            handleClose();
            openCreateFile();
        }
    };

    const saveFieldValue = (text) => {
        createFileNameValue = text;
    };

    const handleCreate = () => {
        const options = { 
            method: 'post',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: createFileNameValue,
                projectType: projectTypeValue
            })
        };
        fetch("../create-project", options)
           .then(res => {
               setCreateOpen(false);
           });
    }

    [projectTypeValue, setProjectTypeValue] = React.useState('Desktop');

    const handleChange = event => {
        setProjectTypeValue(event.target.value);
    };

    let projectCounter = 0;

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar style={{
                    backgroundColor: '#115173',
                    overflowX: 'auto'
                }}>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Elpis IDE
                    </Typography>
                    <div
                        className={classes.leftMenu}>
                        <Button style={{color: 'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            File
                        </Button>
                    </div>
                    <div>
                        <Button style={{color: 'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            Edit
                        </Button>
                    </div>
                    <div>
                        <Button style={{color: 'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            View
                        </Button>
                    </div>
                    <div>
                        <Button style={{color: 'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            Debug
                        </Button>
                    </div>
                    <div>
                        <Button style={{color: 'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            Terminal
                        </Button>
                    </div>
                    <div>
                        <Button style={{color: 'white'}} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            Help
                        </Button>
                    </div>
                    <div style={{
                        flexGrow: 1,
                    }}/>
                    {props.parent.state.width < 700 ? 
                    <IconButton
                        style={{
                            marginRight: 178
                        }}
                        color="inherit"
                        aria-label="Search">
                        <SearchIcon />
                    </IconButton>  : 
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>}
                    <div
                        style={{
                            top: 0,
                            right: 0,
                            height: 64,
                            width: 178,
                            position: 'absolute',
                            background: 'linear-gradient(to right, #3a7bd5, #00d2ff)'
                        }} >
                            <div
                              style={{
                                position: 'absolute',
                                top: '50%',
                                transform: 'translate(0, -30%)'
                              }}>
                        <img
                            style={{
                                width: 20,
                                height: 20,
                                marginLeft: 33,
                                fill: 'black'
                            }} 
                            src={RunIcon}
                            onClick={() => {
                                const options = { 
                                    method: 'post',
                                    headers: {
                                      'Accept': 'application/json, text/plain, */*',
                                      'Content-Type': 'application/json'
                                    }
                                  };
                                  fetch("../run-program", options)
                                   .then(res => {});
                            }}/>
                        <img
                            style={{
                                width: 20,
                                height: 20,
                                marginLeft: 24
                            }} 
                            src={PauseIcon}
                            onClick={() => {
                                const options = {
                                    method: 'post',
                                    headers: {
                                      'Accept': 'application/json, text/plain, */*',
                                      'Content-Type': 'application/json'
                                    }
                                  };
                                  fetch("../kill-current", options)
                                    .then(res => {});;
                            }}/>
                        <img
                            style={{
                                width: 20,
                                height: 20,
                                marginLeft: 24
                            }} 
                            src={StopIcon}
                            onClick={() => {
                                const options = {
                                    method: 'post',
                                    headers: {
                                      'Accept': 'application/json, text/plain, */*',
                                      'Content-Type': 'application/json'
                                    }
                                  };
                                  fetch("../kill-current", options);
                            }}/>
                            </div>
                    </div>
                </Toolbar>
            </AppBar>
            <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                            >
                            <MenuItem onClick={() => onOptionMenuItemSelected(0)}>
                                <ListItemIcon>
                                    <SendIcon fontSize="small" />
                                </ListItemIcon>
                                <Typography variant="inherit">New Project</Typography>
                            </MenuItem>
                            <MenuItem onClick={() => onOptionMenuItemSelected(1)}>
                                <ListItemIcon>
                                    <SendIcon fontSize="small" />
                                </ListItemIcon>
                                <Typography variant="inherit">Projects List</Typography>
                            </MenuItem>
                            <MenuItem onClick={() => onOptionMenuItemSelected(2)}>
                                <ListItemIcon>
                                    <SendIcon fontSize="small" />
                                </ListItemIcon>
                                <Typography variant="inherit">New File</Typography>
                            </MenuItem>
                            <MenuItem onClick={() => onOptionMenuItemSelected(3)}>
                                <ListItemIcon>
                                    <SendIcon fontSize="small" />
                                </ListItemIcon>
                                <Typography variant="inherit">Toggle Theme</Typography>
                            </MenuItem>
                            <MenuItem onClick={() => onOptionMenuItemSelected(4)}>
                                <ListItemIcon>
                                    <SendIcon fontSize="small" />
                                </ListItemIcon>
                                <Typography variant="inherit">Preferences</Typography>
                            </MenuItem>
                            <MenuItem onClick={() => onOptionMenuItemSelected(5)}>
                                <ListItemIcon>
                                    <SendIcon fontSize="small" />
                                </ListItemIcon>
                                <Typography variant="inherit">Exit</Typography>
                            </MenuItem>
                        </Menu>
    <Dialog open={createOpen} onClose={() => setCreateOpen(false)} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Create new project</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter project name:
          </DialogContentText>
          <TextField
            onChange={(event) => saveFieldValue(event.target.value)}
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
          />
          <RadioGroup style={{marginTop: 16}} aria-label="Project Type" name="ProjectType" value={projectTypeValue} onChange={handleChange}>
            <FormControlLabel value="Desktop" control={<Radio />} label="Desktop" />
            <FormControlLabel value="Web" control={<Radio />} label="Web" />
            <FormControlLabel value="Mobile" control={<Radio />} label="Mobile" />
          </RadioGroup>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setCreateOpen(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={() => handleCreate()} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    <Dialog
      maxWidth="xs"
      aria-labelledby="confirmation-dialog-title"
      onClose={() => setProjectsOpen(false)}
      open={projectsOpen}
    >
      <DialogTitle id="confirmation-dialog-title">Projects</DialogTitle>
      <DialogContent dividers>
      <List component="div" role="list">
            {
              projectsList === null ?
               null :
               projectsList.map((projectName) =>{
                 const projectId = projectCounter;
                 projectCounter++;
                 return (<ListItem
                    button
                    divider
                    aria-haspopup="true"
                    aria-label="project"
                    onClick={() => {
                        handleSwitchProject();
                        props.parent.setCode('');
                        props.parent.clearTabs();
                        const options = {
                            method: 'post',
                            headers: {
                              'Accept': 'application/json, text/plain, */*',
                              'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                projectName: projectsList[projectId]
                            })
                        };
                        fetch("../switch-project", options);
                    }}
                    role="listitem"
                  >
                    <ListItemText primary={projectName} secondary={projectName} />
                  </ListItem>)
               })
            }
      </List>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setProjectsOpen(false)} color="primary">
          Ok
        </Button>
      </DialogActions>
    </Dialog>
        </div>
    );
}